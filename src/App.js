// import logo from './logo.svg';
import './App.css';
import React, { useState } from 'react';
import ButtonMerah from './components/button';
import ReCAPTCHA from "react-google-recaptcha";

// const [captcha, setCaptha] = useState(false)

function onChange() {
  // setCaptha(!captcha)
  // console.log(captcha,'recaptcha');
}

const date = new Date();

let day = (date.getDate()).toString().padStart(2, "0");
let month = (date.getMonth() + 1).toString().padStart(2, "0");
let year = date.getFullYear();

let currentDate = `${year}-${day}-${month}`;
// console.log(currentDate,'cek currentDate');

function App() {  
  return (
    <div className="App">
      <h1>RECRUITMENT FORM</h1>
      <form className="App-header" style={{background:"fff"}}>
          <div className="content-wrap">
            <div className="content content-left">
              {/* DATE: GET DARI CURRENT DATE */}
              <label className="label" for="date">DATE:</label>
              <input className="input-style" disabled={true} value={currentDate} type="date" id="date" name="begin"/><br/>

              <label className='label' for="lname">FUll NAME:</label>
              <input className='input-style' type="text" id="lname" name="fullname"/><br/>
              
              {/* CANDIDATE NO: CAN-ROLES 3 HURUF AWAL-DDMMYY5DEIGITNOURUT */}
              <label className="label" for="lname">CANDIDATE NO:</label>
              <input className="input-style" type="text" id="lname" name="lname"/><br/>
              
              {/* ROLES: GET DARI MASTER ROLES DROPDOWN */}
              <label className="label" for="lname" >ROLES:</label>
              <select className="input-roles" name="roles" id="roles">
                <option value="null"> </option>
                <option value="sales">SALES</option>
                <option value="marketing">MARKETING</option>
                <option value="cs">CUSTOMER SERVICE</option>
                <option value="bd">BUSINESS DEVELOPMENT</option>
                <option value="courier">COURIER</option>
              </select><br/>

              {/* PHONE NO: MANUAL INPUT */}
              <label className="label" for="phone">PHONE NO:</label>
              <input style={{width:"98%"}} className="input-style" type="number" id="phone" name="phone" min={6} max={14}/><br/>
            
              {/* EMAIL: MANUAL INPUT */}
              <label className="label" for="email">EMAIL:</label>
              <input className="input-style" type="email" id="email" name="email"/><br/>
            
              {/* GENDER: DROPDOWN, MALE FEMALE */}
              <label className="label" for="lname">GENDER:</label>
              <div className="content-gender">
                <ButtonMerah label="MALE"/>
                <ButtonMerah label="FEMALE"/>
                {/* <button value="male" style={{padding:"0 10%"}}>MALE</button>
                <button value="female" style={{marginLeft:"20px",padding:"0 10%", height:24}}>FEMALE</button> */}
              </div><br/>
            
              {/* CURRENT ADDRES: MANUAL INPUT */}
              <label className="label" for="current-address">CURRENT ADDRES:</label>
              <textarea id="address" name="address" rows="4" cols="50" className="address" placeholder="Please Input Your Address..."> </textarea>
              {/* <input type="text" id="current-address" name="current-address"/><br/> */}
            </div>

            <div className='content content-right'>
              {/* BIRTH DATE: DATE PICKER */}
              <label className="label" for="date">BIRTH DATE:</label>
              <input className="input-style" placeholder="dd-mm-yyy" type="date" id="date" name="begin"/><br/>
            
              {/* REFERENCE: MANUAL INPUT */}
              <label className="label" for="date">REFERENCE:</label>
              <input className="input-style" type="text" id="reference" name="reference"/><br/>
            
              {/* PREVIOUS COMPANY: MANUAL INPUT */}
              <label className="label" for="lname">PREVIOUS COMPANY:</label>
              <input className="input-style" type="text" id="lname" name="lname"/><br/>
            
              {/* CV UPLOAD: UPLOAD, ONLY ACCEPT PDF & DOCX (MAX 5MB) */}
              <label className="label" for="cvupload">CV UPLOAD:</label>
              {/* <input type="file" id="cvupload" name="cvupload"/><br/> */}
              <input type="file" name="upload" accept="application/pdf" /><br/>

              <ReCAPTCHA
                sitekey="6LeCbFckAAAAAF07S51ir-Y-1aRMIjeNwWC_Wma7"
                onChange={onChange}
              />
              <ButtonMerah disabled={false} onClick={() => alert("SUCCESS")} label="SAVE"/>
              
              {/* <input onClick={alert()} type="submit" value="Submit" style={{width:"50%",alignSelf:"center"}}/> */}
            </div>
          </div>
      </form>
    </div>
  );
}

export default App;
