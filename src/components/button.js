import {Button, Text} from '@chakra-ui/react'
import colors from '../components/colors'

function ButtonMerah({disabled, onClick, label: LabelNya, type, ...props}) {
    const labelIsString = typeof LabelNya === 'string'
    return (
        <Button
            _hover={{bg: colors.MERAH_TERANG}}
            id="submit-red-btn"
            disabled={disabled}
            type={type}
            mt="2"
            size="sm"
            bg={disabled ? colors.BORDER_GRAY : colors.COLOR_WHITE}
            rounded="6px"
            border="#BD0C1Fed"
            outline="none"
            color="none"
            className="btn btn-sm btn-success btn-vertical-top"
            // title="Tambah"
            onClick={onClick}
            style={{padding:"0 14%"}}
            px="8"
            {...props}
        >
            {labelIsString ? (
                <Text fontSize="11px" fontWeight="bold">
                    {LabelNya}
                </Text>
            ) : (
                LabelNya
            )}
        </Button>
    )
}

export default ButtonMerah
