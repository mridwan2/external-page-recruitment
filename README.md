# External Page Recruitment
External Page Recruitment is a front end project to display recruitment form

## Installation

Use the package manager [npm](https://nodejs.org/en/) to install External Page Recruitment.

```bash
npm install
```

## Usage

```javascript
npm start

```

## Contributing


Pull requests are welcome. For major changes, please create new branch with your name, then create an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.
